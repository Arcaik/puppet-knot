type Knot::Boolean = Variant[Boolean, Enum['on', 'off', 'true', 'false']] # lint:ignore:quoted_booleans
