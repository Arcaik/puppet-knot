type Knot::Module = Struct[{
  id   => String,
  file => Optional[Stdlib::Absolutepath],
}]
