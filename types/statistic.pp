type Knot::Statistic = Struct[{
  timer  => Knot::Time,
  file   => Optional[Stdlib::Absolutepath],
  append => Optional[Knot::Boolean],
}]
