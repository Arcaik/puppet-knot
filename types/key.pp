type Knot::Key = Struct[{
  id        => Stdlib::Fqdn,
  algorithm => Enum['hmac-md5', 'hmac-sha1', 'hmac-sha224', 'hmac-sha256', 'hmac-sha384', 'hmac-sha512'],
  secret    => Stdlib::Base64,
}]
