type Knot::Submission = Struct[{
  id               => String,
  parent           => Variant[String, Array[String]],
  'check-interval' => Optional[Knot::Time],
  timeout          => Optional[Knot::Time],
}]
