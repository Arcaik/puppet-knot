type Knot::Remote = Struct[{
  id      => String,
  address => Variant[String, Array[String]], # IP Address + Port
  via     => Optional[Variant[String, Array[String]]], # IP Address + Port
  key     => Optional[String],
}]
