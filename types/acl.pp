type Knot::ACL = Struct[{
  id      => String,
  address => Optional[Variant[Stdlib::IP::Address, Array[Stdlib::IP::Address]]],
  key     => Optional[Variant[String, Array[String]]],
  action  => Optional[Variant[Knot::ACL::Action, Array[Knot::ACL::Action]]],
  deny    => Optional[Knot::Boolean],
}]
