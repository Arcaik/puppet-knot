type Knot::Log = Struct[{
  target  => Enum['stdout', 'stderr', 'syslog', 'String'],
  server  => Optional[Knot::Log::Severity],
  control => Optional[Knot::Log::Severity],
  zone    => Optional[Knot::Log::Severity],
  any     => Optional[Knot::Log::Severity],
}]
