type Knot::Control = Struct[{
  listen  => Optional[Stdlib::Absolutepath],
  timeout => Optional[Knot::Time],
}]
