type Knot::Log::Severity = Enum['critical', 'error', 'warning', 'notice', 'info', 'debug']
