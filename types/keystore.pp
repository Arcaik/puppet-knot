type Knot::Keystore = Struct[{
  id      => String,
  backend => Optional[Enum['pem', 'pkcs11']],
  config  => Optional[String],
}]
