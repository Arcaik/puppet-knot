require 'spec_helper'

describe 'knot' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end

      it { is_expected.to compile.with_all_deps }

      context 'with default values for all parameters' do
        it do
          is_expected.to contain_class('knot::install')
            .that_comes_before('Class[knot::config]')
        end

        it do
          is_expected.to contain_class('knot::config')
            .that_notifies('Class[knot::service]')
        end

        it { is_expected.to contain_class('knot::service') }

        # knot::install
        it do
          is_expected.to contain_package('knot')
            .with(
              'ensure' => 'installed',
            )
        end

        # knot::config
        it do
          is_expected.to contain_file('/etc/knot')
            .with(
              'ensure'  => 'directory',
              'owner'   => 'knot',
              'group'   => 'knot',
              'mode'    => '0750',
            )
        end

        it do
          is_expected.to contain_file('/etc/knot/knot.conf')
            .with(
              'ensure' => 'file',
              'owner'  => 'knot',
              'group'  => 'knot',
              'mode'   => '0640',
            )
        end

        it do
          is_expected.to contain_file('/var/lib/knot')
            .with(
              'ensure'  => 'directory',
              'owner'   => 'knot',
              'group'   => 'knot',
              'mode'    => '0750',
            )
        end

        # knot::service
        it do
          is_expected.to contain_service('knot')
            .with(
              'ensure' => 'running',
              'enable' => true,
            )
        end
      end
    end
  end
end
