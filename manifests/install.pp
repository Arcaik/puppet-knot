# @summary
#   Manages Knot installation
#
# @api private
class knot::install {
  package { 'knot':
    ensure => 'installed',
  }
}
