# @summary
#   Manage Knot installation and configuration
#
# @see
#   the [Knot documentation](https://www.knot-dns.cz/docs/2.7/html/reference.html)
#   for details on configuration options.
#
# @param include
#   Path to another file (or a glob pattern) that will be included by Knot.
#
# @param modules
#   Settings set in the modules section of the configuration file.
#
# @param server
#   Settings set in the server section of the configuration file.
#
# @option server [Variant[Array[String], String]] listen
#   Default value: `[ '0.0.0.0@53', '::@53' ]`
#
# @option server [Pattern[/^[[:alnum:]_][[:alnum:]_-]*(:[[:alnum:]_][[:alnum:]_-]*)?$/]] user
#   Default value: `'knot:knot'`,
#
# @param keys
#   Settings set in the keys section of the configuration file.
#
# @param acls
#   Settings set in the acls section of the configuration file.
#
# @param control
#   Settings set in the control section of the configuration file.
#
# @param statistics
#   Settings set in the statistics section of the configuration file.
#
# @param keystore
#   Settings set in the keystore section of the configuration file.
#
# @param submission
#   Settings set in the submission section of the configuration file.
#
# @param policies
#   Settings set in the policies section of the configuration file.
#
# @param remotes
#   Settings set in the remotes section of the configuration file.
#
# @param templates
#   Settings set in the templates section of the configuration file.
#
# @param zones
#   Settings set in the zones section of the configuration file.
#
# @param log
#   Settings set in the log section of the configuration file.
#
class knot (
  Optional[String]                  $include    = undef,
  Optional[Array[Knot::Module]]     $modules    = undef,
  Optional[Knot::Server]            $server     = undef,
  Optional[Array[Knot::Key]]        $keys       = undef,
  Optional[Array[Knot::ACL]]        $acls       = undef,
  Optional[Knot::Control]           $control    = undef,
  Optional[Knot::Statistic]         $statistics = undef,
  Optional[Array[Knot::Keystore]]   $keystore   = undef,
  Optional[Array[Knot::Submission]] $submission = undef,
  Optional[Array[Knot::Policy]]     $policies   = undef,
  Optional[Array[Knot::Remote]]     $remotes    = undef,
  Optional[Array[Knot::Template]]   $templates  = undef,
  Optional[Array[Knot::Zone]]       $zones      = undef,
  Optional[Array[Knot::Log]]        $log        = undef,
  ) {
  contain ::knot::install
  contain ::knot::config
  contain ::knot::service

  Class['::knot::install']
  -> Class['::knot::config']
  ~> Class['::knot::service']
}
