# @summary
#   Manages Knot service
#
# @api private
#
class knot::service {
  service { 'knot':
    ensure => running,
    enable => true,
  }
}
