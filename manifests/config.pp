# @summary
#   Manages Knot configuration
#
# @api private
#
class knot::config {
  $knot_default_settings = {
    server => {
      listen => [ '0.0.0.0@53', '::@53' ],
      user   => 'knot:knot',
    },
    log    => {
      syslog => {
        any => 'info',
      },
    },
  }

  $knot_settings = merge($knot_default_settings, {
    include    => $::knot::include,
    module     => $::knot::modules,
    server     => $::knot::server,
    key        => $::knot::keys,
    acl        => $::knot::acls,
    control    => $::knot::control,
    statistic  => $::knot::statistics,
    keystore   => $::knot::keystore,
    submission => $::knot::submission,
    policy     => $::knot::policies,
    remote     => $::knot::remotes,
    template   => $::knot::templates,
    zone       => $::knot::zones,
    log        => $::knot::log,
  })

  file {
    default:
      owner => 'knot',
      group => 'knot',
    ;
    '/etc/knot':
      ensure => directory,
      mode   => '0750',
    ;
    '/etc/knot/knot.conf':
      ensure       => file,
      mode         => '0640',
      content      => epp('knot/knot.conf.epp', $knot_settings),
      validate_cmd => '/usr/sbin/knotc -c % conf-check'
    ;
    '/var/lib/knot':
      ensure => directory,
      mode   => '0750',
    ;
  }
}
