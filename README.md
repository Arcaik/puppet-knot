# Knot

[![CI/CD status](https://gitlab.com/Arcaik/puppet-knot/badges/master/pipeline.svg)](https://gitlab.com/Arcaik/puppet-knot/commits/master)

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with knot](#setup)
    * [What this module affects](#what-this-module-affects)
    * [Beginning with this module](#beginning-with-this-module)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module manages the installation and configuration of Knot, a
high-performance authoritative-only DNS server.

## Setup

### What this module affects

* Knot package installation
* Knot configuration in /etc/knot/knot.conf

### Beginning with this module

```puppet
include ::knot
```

## Usage

## Reference

See [REFERENCE.md](https://gitlab.com/Arcaik/puppet-knot/blob/master/REFERENCE.md).

## Limitations

This module has only been tested on Debian 9 (Strech).

## Development

Contributions are really welcome. Feel free to propose your merge request or to
open an issue.
